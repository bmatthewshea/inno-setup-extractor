unit Unit3AboutForm;

{$mode objfpc}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  lclintf, ExtCtrls, vinfo;

type

  { TForm3About }

  TForm3About = class(TForm)
    ButtonAboutDonate: TButton;
    Label1: TLabel;
    Label2: TLabel;
    LabelGNU: TLabel;
    LabelInnoUnp: TLabel;
    LabelInnoSetup: TLabel;
    LabelAboutAuthor: TLabel;
    LabelAboutVersion: TLabel;
    LabelAboutTitle: TLabel;
    LabelAboutDate: TLabel;
    ButtonAboutOK: TButton;
    LabelAboutContact: TLabel;
    LabelAboutEmail: TLabel;
    LabelAboutWeb: TLabel;
    LabelAboutWebSite: TLabel;
    Shape1: TShape;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    procedure ButtonAboutDonateClick(Sender: TObject);
    procedure ButtonAboutOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LabelGNUClick(Sender: TObject);
    procedure LabelInnoSetupClick(Sender: TObject);
    procedure LabelInnoUnpClick(Sender: TObject);
    procedure URLLabelMouseDown(Sender: TObject);
    procedure URLLabelMouseDownURL(sURL: String);
    procedure URLLabelMouseEnter(Sender: TObject);
    procedure URLLabelMouseLeave(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  Form3About: TForm3About;

implementation

{$R *.lfm}

procedure TForm3About.URLLabelMouseDown(Sender: TObject);//  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  OpenURL(TLabel(Sender).Caption);
end;

procedure TForm3About.URLLabelMouseDownURL(sURL: String);
begin
  If (sURL<>'') AND (pos('http://', sURL)=1)
  then OpenURL(sURL);
end;

procedure TForm3About.ButtonAboutOKClick(Sender: TObject);
begin
  Close;
end;

procedure TForm3About.FormCreate(Sender: TObject);
var
  BuildNum : String;
  Info: TVersionInfo;
begin
  Info := TVersionInfo.Create;
  Info.Load(HINSTANCE);
  // grab just the Build Number
  BuildNum := IntToStr(Info.FixedInfo.FileVersion[0]) + '.' + IntToStr(Info.FixedInfo.FileVersion[3]);
  Info.Free;
  //update ABOUT form version
  Form3About.LabelAboutVersion.Caption := 'v'+BuildNum;
end;

procedure TForm3About.LabelGNUClick(Sender: TObject);
begin
   URLLabelMouseDownURL('http://www.gnu.org/copyleft/gpl.html');
end;

procedure TForm3About.LabelInnoSetupClick(Sender: TObject);
begin
  URLLabelMouseDownURL('http://www.jrsoftware.org/isinfo.php');
end;

procedure TForm3About.LabelInnoUnpClick(Sender: TObject);
begin
  URLLabelMouseDownURL('http://innounp.sourceforge.net/');
end;

procedure TForm3About.ButtonAboutDonateClick(Sender: TObject);
begin
  OpenURL('https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=HVBFGL7SBXBMC&lc=US&item_name=Holy%20Linux%20Web%20Site&item_number=innounpgui&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_LG%2egif%3aNonHosted');
end;

procedure TForm3About.URLLabelMouseLeave(Sender: TObject);
begin
  TLabel(Sender).Font.Style := [];
  TLabel(Sender).Font.Color := clBlue;
  TLabel(Sender).Cursor := crDefault;
end;

procedure TForm3About.URLLabelMouseEnter(Sender: TObject);
begin
  TLabel(Sender).Font.Style := [fsUnderLine];
  TLabel(Sender).Font.Color := clRed;
  TLabel(Sender).Cursor := crHandPoint;
end;



end.

