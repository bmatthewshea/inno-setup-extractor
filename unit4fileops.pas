unit Unit4FileOps;

{$mode objfpc}{$LONGSTRINGS ON}

interface

uses
   Classes, SysUtils, LResources, FileUtil, Forms, Controls, Graphics, Dialogs,
  Menus, Buttons, LCLType, ExtCtrls, ActnList, shfolder, Process;

   function  ExecuteFileInno(ExeName : String) : integer;
   procedure SaveMyFile(AStringList : TStringList);
   function  GetSpecialFolderPath(folder : integer) : string;
   function  RandomFileName() : string;

implementation

function ExecuteFileInno(ExeName: String) : integer;
const
//  READ_BYTES = 2048;
  READ_BYTES = 2048;
var
  S: TStringList;
  M: TMemoryStream;
  P: TProcess;
  n: LongInt;
  BytesRead: LongInt;
  sExecutable, sOptions, sTempFileName: String;
//  sPipeExecutable: String;
  fOutputFile : TextFile;

begin
  sExecutable:='./innounp.exe';
  sOptions:=' -v'; //important - leave space before option...
  sTempFileName := GetTempDir(False)+'\'+RandomFileName();
  M := TMemoryStream.Create;
  BytesRead := 0;
  P := TProcess.Create(nil);
  //
  P.CommandLine := sExecutable + sOptions + ' ' + ExeName;
  P.Options := [poUsePipes, poStderrToOutPut, poNoConsole];
  //
  Assign(fOutputFile,sTempFileName);
  Rewrite(fOutputFile);              //write temp file to count lines
  //WriteLn(fOutputFile, '-- executing --');
  //ShowMessage(P.CommandLine);
  P.Execute;                         //execute process

  while P.Running do
  begin
    // make sure we have room
    M.SetSize(BytesRead + READ_BYTES);
    // try reading it
    n := P.Output.Read((M.Memory + BytesRead)^, READ_BYTES);
    if n > 0
    then begin
      Inc(BytesRead, n);
    //Write(fOutputFile, '.')
    end
    else begin
    // no data, wait 100 ms
      Sleep(100);
    end;
  end;
    // read last part
  repeat
    // make sure we have room
    M.SetSize(BytesRead + READ_BYTES);
    // try reading it
    n := P.Output.Read((M.Memory + BytesRead)^, READ_BYTES);
    if n > 0
    then begin
      Inc(BytesRead, n);
    //Write('.');
    end;
  until n <= 0;
  if BytesRead > 0 then WriteLn(fOutputFile);
  M.SetSize(BytesRead);
  //WriteLn(fOutputFile, '-- executed --');
  S := TStringList.Create;
  S.LoadFromStream(M);
  for n := 0 to S.Count - 1 do
  begin
    WriteLn(fOutputFile, '| ', S[n]);
  end;
  WriteLn(fOutputFile);
  CloseFile(fOutputFile);
  Result:=S.Count-7;
  DeleteFile(sTempFileName);
  S.Free;
  P.Free;
  M.Free;
end;

procedure SaveMyFile(AStringList: TStringList);
var
SDiag : TSaveDialog;
begin

   SDiag := TSaveDialog.Create(NIL);
   //try
   SDiag.Title := 'Save';
   SDiag.InitialDir := (GetSpecialFolderPath(CSIDL_PERSONAL) + '\');
   SDiag.DefaultExt := 'txt';
   SDiag.FilterIndex := 1;
   Sdiag.Filter:= 'Text file|*.txt';
   //Sdiag.Options:=
     if SDiag.Execute then
     AStringList.SaveToFile(SDiag.FileName);
   //ShowMessage(AStringList.Text + 'is what would have saved...' + ' to ' +#13#13+ Sdiag.FileName);
   //finally
   FreeAndNil(SDiag);
   //end;

end;

function GetSpecialFolderPath(folder : integer) : string;
 const
   SHGFP_TYPE_CURRENT = 0;
 var
   path: array [0..MAX_PATH] of char;
   i:hresult;
 begin
   i:= SHGetFolderPath(0,folder,0,SHGFP_TYPE_CURRENT,@path[0]);
   if i = S_OK then
     Result := path
   else
     Result := GetCurrentDir;
 end;
function RandomFileName() : string;
const chars = '1234567890abcdefghijklmnopqrstuvwxyz';
var sRandomFilename : string;
    TempInt : integer;
begin
 sRandomFileName := '';
 Randomize;
  for TempInt := 1 to 8 do sRandomFileName := sRandomFileName + chars[Random(36)+1];
  result := sRandomFileName

end;


end.

