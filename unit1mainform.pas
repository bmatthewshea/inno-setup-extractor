unit Unit1MainForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, FileUtil, Forms, Controls, Graphics, Dialogs,
  StdCtrls, Buttons, EditBtn, Menus, lclintf, ExtCtrls, Unit2OutputForm, Unit3AboutForm,
  Unit4FileOps, inifiles, shfolder;

procedure WriteINI();
procedure ReadINI();

type

  { TForm1Main }

  TForm1Main = class(TForm)
    Actions: TMenuItem;
    ButtonLoadDefaults: TButton;
    ButtonUnpack: TButton;
    ButtonCloseOptions: TButton;
    CheckBoxSaveOnExit: TCheckBox;
    CheckBoxNoWarnings: TCheckBox;
    DirectoryEditExtractFolder: TDirectoryEdit;
    EditPasswordBox: TEdit;
    ExitProgram: TMenuItem;
    FileMenu: TMenuItem;
    FileMenuDashMark: TMenuItem;
    FileNameEditFile: TFileNameEdit;
    FileNameEditPasswordFile: TFileNameEdit;
    Help: TMenuItem;
    HelpAboutProgram: TMenuItem;
    HelpProgram: TMenuItem;
    MainMenu: TMainMenu;
    MainMenuOpenDialog: TOpenDialog;
    OpenFile: TMenuItem;
    OpenRecent: TMenuItem;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Shape4: TShape;
    Shape5: TShape;
    StaticText1: TStaticText;
    StaticTextMainOptions: TStaticText;
    StaticTextDragDrop: TStaticText;
    StaticTextExtractWithoutPaths: TStaticText;
    StaticTextInnoSetupFile: TStaticText;
    StaticTextDirectoryOverride: TStaticText;
    StaticTextMainPasswords: TStaticText;
    StaticTextMiscellaneous: TStaticText;
    StaticTextProcessAllDupsOption: TStaticText;
    StaticTextSpecifyPasswordFile: TStaticText;
    StaticTextNoNewVersionsOption: TStaticText;
    StaticTextExtractDirectoryOption: TStaticText;
    StaticTextPasswordOption: TStaticText;
    StaticTextExtractEmbeddedOption: TStaticText;
    StaticTextBatchModeOption: TStaticText;
    StaticTextTestFileOption: TStaticText;
    StaticTextExtractFiles: TStaticText;
    StaticTextVerbose: TStaticText;
    StaticTextFileInfoOnlyOption: TStaticText;
    ToggleBoxFileInfoOnly: TToggleBox;
    ToggleBoxTestFiles: TToggleBox;
    ToggleBoxNoNewVersions: TToggleBox;
    ToggleBoxProcessAllDuplicates: TToggleBox;
    ToggleBoxBatchMode: TToggleBox;
    ToggleBoxVerbose: TToggleBox;
    ToggleBoxExtractEmbedded: TToggleBox;
    ToggleBoxExtractFilesNormal: TToggleBox;
    ToggleBoxExtractWithoutPaths: TToggleBox;
    UnpackNormal: TMenuItem;
    procedure ButtonCloseOptionsClick(Sender: TObject);
    procedure ButtonLoadDefaultsClick(Sender: TObject);
    procedure ButtonUnpackClick(Sender: TObject);
    procedure ExitProgramClick(Sender: TObject);
    procedure FileNameEditFileChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of String);
    procedure HelpAboutProgramClick(Sender: TObject);
    procedure HelpProgramClick(Sender: TObject);
    procedure OpenFileClick(Sender: TObject);
    procedure ToggleBoxBatchModeChange(Sender: TObject);
    procedure ToggleBoxExtractEmbeddedChange(Sender: TObject);
    procedure ToggleBoxExtractFilesNormalChange(Sender: TObject);
    procedure ToggleBoxExtractWithoutPathsChange(Sender: TObject);
    procedure ToggleBoxFileInfoOnlyChange(Sender: TObject);
    procedure ToggleBoxNoNewVersionsChange(Sender: TObject);
    procedure ToggleBoxProcessAllDuplicatesChange(Sender: TObject);
    //procedure ToggleBoxTestFiles(Sender: TObject);
    procedure ToggleBoxTestFilesChange(Sender: TObject);
    procedure ToggleBoxVerboseChange(Sender: TObject);
    procedure UnpackNormalClick(Sender: TObject);
    procedure GreyFormButtons(Sender: TObject);
    procedure UnGreyFormButtons(Sender: TObject);
  private
    { private declarations }
  public

  end; 

var
  Form1Main: TForm1Main;
  FileInfoOnly: Boolean;
  TestFilesOnly: Boolean;
  //FullFileName: String;
  //ShortFileName: String;

implementation

{$R *.lfm}

{ TForm1Main }

procedure TForm1Main.FormCreate(Sender: TObject);
begin
// Load last saved INI
  ReadINI();
   If FileNameEditFile.FileName='' then
   begin
     ButtonUnpack.Enabled:=False;
     UnpackNormal.Enabled:=False;
   end else
   begin
     ButtonUnpack.Enabled:=True;
     UnpackNormal.Enabled:=True;
   end;
end;

procedure TForm1Main.ButtonCloseOptionsClick(Sender: TObject);
begin
   if (CheckBoxSaveOnExit.State = cbChecked) then WriteINI();
   Close;
end;

procedure TForm1Main.ButtonLoadDefaultsClick(Sender: TObject);
begin
  FileNameEditFile.FileName := '';
  ToggleBoxVerbose.Checked := False;
  ToggleBoxExtractFilesNormal.Checked := True;
  ToggleBoxExtractWithoutPaths.Checked := False;
  ToggleBoxExtractEmbedded.Checked := True;
  ToggleBoxTestFiles.Checked := True;
  DirectoryEditExtractFolder.Directory := '';
  EditPasswordBox.Text := '';
  FileNameEditPasswordFile.FileName := '';
  ToggleBoxNoNewVersions.Checked := False;
  ToggleBoxProcessAllDuplicates.Checked := False;
  ToggleBoxBatchMode.Checked := False;
  ToggleBoxFileInfoOnly.Checked := False;
   If FileNameEditFile.FileName='' then
   begin
     ButtonUnpack.Enabled:=False;
     UnpackNormal.Enabled:=False;
   end else
   begin
     ButtonUnpack.Enabled:=True;
     UnpackNormal.Enabled:=True;
   end;
end;

procedure TForm1Main.ButtonUnpackClick(Sender: TObject);
var
  sExecThis: String;
  sChangeCaption: String;
  sOptions: String;
  sAddOptions: String;
  sPath: String;
  sExe: String;
  iLines: Integer;
  bNoWarnings: Boolean;
begin
  begin
  sPath:=ExtractFileDir(FileNameEditFile.FileName);
  sExe:=ExtractFileName(FileNameEditFile.FileName);
  if sExe = '' then exit;
  end;
  //ShowMessage(sPath);
  sExecThis:=AnsiQuotedStr(FileNameEditFile.FileName, chr(34));
  sAddOptions:='-';
  sOptions:=' ';
  bNoWarnings:=CheckBoxNoWarnings.Checked;
  // Build Setup Options For Inno

  If (ToggleBoxFileInfoOnly.State = cbUnchecked) then
  begin
    if (ToggleBoxVerbose.State = cbChecked) then              sAddOptions:=sAddOptions+'v';
    if (ToggleBoxExtractFilesNormal.State = cbChecked) then   sAddOptions:=sAddOptions+'x';
    if (ToggleBoxExtractWithoutPaths.State = cbChecked) then  sAddOptions:=sAddOptions+'e';
    if (ToggleBoxTestFiles.State = cbChecked) then            sAddOptions:=sAddOptions+'t';
    sAddOptions:=sAddOptions+' ';
    if (ToggleBoxExtractEmbedded.State = cbChecked) then      sAddOptions:=sAddOptions+'-m ';
    if (ToggleBoxNoNewVersions.State = cbChecked) then        sAddOptions:=sAddOptions+'-n ';
    if (ToggleBoxProcessAllDuplicates.State = cbChecked) then sAddOptions:=sAddOptions+'-a ';
    if (ToggleBoxBatchMode.State = cbChecked) then            sAddOptions:=sAddOptions+'-b ';
    sAddOptions:=sAddOptions+'-y ';
    if DirectoryEditExtractFolder.Directory<>'' then
    sAddOptions:=sAddOptions+'-d' + AnsiQuotedStr(DirectoryEditExtractFolder.Directory, chr(34)) + ' '
      else sAddOptions:=sAddOptions+'-d'+ AnsiQuotedStr(sPath + '\'+sExe+'-setup', chr(34)) + ' ';
    if EditPasswordBox.Text<>'' then
    sAddOptions:=sAddOptions+'-p'  + EditPasswordBox.Text + ' '
      else if FileNameEditPasswordFile.FileName<>'' then
    sAddOptions:=sAddOptions+'-f ' + AnsiQuotedStr(FileNameEditPasswordFile.FileName, chr(34));
    sOptions:=sOptions+sAddOptions;
  end
  else
  begin
    If (ToggleBoxBatchMode.State = cbChecked) then sOptions := ' -b'
    else sOptions := '';
  end;


  // Setup OUTPUT Form
  Form2Output.ButtonOutputOkClose.Enabled:=False;
  Form2Output.ButtonOutputSave.Enabled:=False;
  Form2Output.ButtonCopyForm.Enabled:=False;
  Form2Output.ButtonClearForm.Enabled:=False;
  Form2Output.MemoOutput.Clear; {CLEAR SLATE}
  //Form2Output.MemoOutput.ReadOnly:=True; {NO EDITING}
  sChangeCaption:=Form2Output.Caption; {SAVE CURRENT CAPTION}
  Form2Output.Caption:=Form2Output.Caption + ' - Working..  Please Wait';
  Form2Output.ProgressBarUnpack.Visible:=True;
  Form2Output.Show;

  try
   //EXECUTE
    iLines:=ExecuteFileInno(sExecThis);        //creates quick temp file listing only - makes count of (iLines)..
    ExecInno(sExecThis, sOptions, bNoWarnings, iLines);    //Execute again but with user options - send lines for progress bar.
   //.......
  Finally
   if (ToggleBoxVerbose.State = cbChecked) AND (Form2Output.MemoOutput.Lines.Count>6) then
   begin
     Form2Output.MemoOutput.Lines.Text := Form2Output.MemoOutput.Lines.Text + #13#10 + 'Files Listed = ' + IntToStr(Form2Output.MemoOutput.Lines.Count-7);
     Form2Output.MemoOutput.SelStart := Length(Form2Output.MemoOutput.Lines.Text);
   end;
   Form2Output.Caption:=sChangeCaption;
   Form2Output.ProgressBarUnpack.Visible:=False;
   Form2Output.ButtonOutputOkClose.Enabled:=True;
   If Form2Output.MemoOutput.Lines.Text <> '' then
   begin
   Form2Output.ButtonOutputSave.Enabled:=True;
   Form2Output.ButtonCopyForm.Enabled:=True;
   Form2Output.ButtonClearForm.Enabled:=True;
   Form2Output.ButtonOutputOkClose.SetFocus;
   end;
 end;
end;



procedure TForm1Main.ExitProgramClick(Sender: TObject);
begin
  Close;
  Exit;
end;

procedure TForm1Main.FileNameEditFileChange(Sender: TObject);
begin
   If FileNameEditFile.FileName='' then
   begin
     ButtonUnpack.Enabled:=False;
     UnpackNormal.Enabled:=False;
   end else
   begin
     ButtonUnpack.Enabled:=True;
     UnpackNormal.Enabled:=True;
   end;
end;

procedure TForm1Main.FormDropFiles(Sender: TObject; const FileNames: array of String);
var
  FullFileName: String;
begin
    if (High(FileNames) >= 0) AND ((ExtractFileExt(FileNames[0])='.exe') OR (ExtractFileExt(FileNames[0])='.EXE')) then
    begin
      FullFileName:= FileNames[0];
      //ShortFileName:= ExtractFileName(FileNames[0]);
      //StaticTextFileName.Caption := 'For file: ' + ShortFileName;
      FileNameEditFile.FileName := FullFileName;
      ButtonUnpackClick(Sender);
    end;
end;

procedure TForm1Main.HelpAboutProgramClick(Sender: TObject);
begin
    Form3About.Showmodal;
end;

procedure TForm1Main.HelpProgramClick(Sender: TObject);
begin
  OpenURL('innounp.htm');
end;

procedure TForm1Main.OpenFileClick(Sender: TObject);
var
  FullFileName : string;
begin
  {OPEN FILE}
   //OpenDialogExtractToFolder.InitialDir := (GetSpecialFolderPath(CSIDL_PERSONAL) + '\');
   MainMenuOpenDialog.DefaultExt := 'exe';
   MainMenuOpenDialog.FilterIndex := 1;
   MainMenuOpenDialog.Filter:= 'Inno Setup Installers|*.exe';
       if MainMenuOpenDialog.Execute then
       begin
     FullFileName := MainMenuOpenDialog.Filename;
     FileNameEditFile.FileName:=FullFileName;
     end;
end;

procedure TForm1Main.ToggleBoxBatchModeChange(Sender: TObject);
begin
       if (ToggleBoxBatchMode.State = cbUnchecked) then
       ToggleBoxBatchMode.Caption:='Off'
       else ToggleBoxBatchMode.Caption:='On';
end;

procedure TForm1Main.ToggleBoxExtractEmbeddedChange(Sender: TObject);
begin
       if (ToggleBoxExtractEmbedded.State = cbUnchecked) then
       ToggleBoxExtractEmbedded.Caption:='Off'
       else ToggleBoxExtractEmbedded.Caption:='On';
end;

procedure TForm1Main.ToggleBoxExtractFilesNormalChange(Sender: TObject);
begin
       if (ToggleBoxExtractFilesNormal.State = cbUnchecked) then
       ToggleBoxExtractFilesNormal.Caption:='Off'
       else ToggleBoxExtractFilesNormal.Caption:='On';
end;

procedure TForm1Main.ToggleBoxExtractWithoutPathsChange(Sender: TObject);
begin
       if (ToggleBoxExtractWithoutPaths.State = cbUnchecked) then
       ToggleBoxExtractWithoutPaths.Caption:='Off'
       else ToggleBoxExtractWithoutPaths.Caption:='On';
end;

procedure TForm1Main.ToggleBoxFileInfoOnlyChange(Sender: TObject);
begin
     if (ToggleBoxFileInfoOnly.State = cbUnchecked) then
       begin
         FileInfoOnly:=False;
         ToggleBoxFileInfoOnly.Caption:='Off';
         UnGreyFormButtons(Sender);
       end
     else
       begin
         FileInfoOnly:=True;
         ToggleBoxFileInfoOnly.Caption:='On';
         GreyFormButtons(Sender);
       end;
end;

procedure TForm1Main.ToggleBoxNoNewVersionsChange(Sender: TObject);
begin
       if (ToggleBoxNoNewVersions.State = cbUnchecked) then
       ToggleBoxNoNewVersions.Caption:='Off'
       else ToggleBoxNoNewVersions.Caption:='On';
end;

procedure TForm1Main.ToggleBoxProcessAllDuplicatesChange(
  Sender: TObject);
begin
       if (ToggleBoxProcessAllDuplicates.State = cbUnchecked) then
       ToggleBoxProcessAllDuplicates.Caption:='Off'
       else ToggleBoxProcessAllDuplicates.Caption:='On';
end;

procedure TForm1Main.ToggleBoxTestFilesChange(Sender: TObject);
begin
       if (ToggleBoxTestFiles.State = cbUnchecked) then
       ToggleBoxTestFiles.Caption:='Off'
       else ToggleBoxTestFiles.Caption:='On';
end;

procedure TForm1Main.ToggleBoxVerboseChange(Sender: TObject);
begin
       if (ToggleBoxVerbose.State = cbUnchecked) then
       ToggleBoxVerbose.Caption:='Off'
       else ToggleBoxVerbose.Caption:='On';
end;

procedure TForm1Main.UnpackNormalClick(Sender: TObject);
begin
  ButtonUnpackClick(Sender);
end;

procedure TForm1Main.GreyFormButtons(Sender: TObject);
begin
  WriteINI(); //Record Settings
  ToggleBoxVerbose.Checked:=False;
  ToggleBoxVerbose.Enabled:=False;

  ToggleBoxExtractFilesNormal.Checked:=False;
  ToggleBoxExtractFilesNormal.Enabled:=False;

  ToggleBoxExtractWithoutPaths.Checked:=False;
  ToggleBoxExtractWithoutPaths.Enabled:=False;

  ToggleBoxExtractEmbedded.Checked:=False;
  ToggleBoxExtractEmbedded.Enabled:=False;

  ToggleBoxTestFiles.Checked:=False;
  ToggleBoxTestFiles.Enabled:=False;

  //DirectoryEditExtractFolder.Directory:='';
  DirectoryEditExtractFolder.Enabled:=False;

  //EditPasswordBox.Text:='';
  EditPasswordBox.Enabled:=False;

  //FileNameEditPasswordFile.FileName:='';
  FileNameEditPasswordFile.Enabled:=False;

  ToggleBoxNoNewVersions.Checked:=False;
  ToggleBoxNoNewVersions.Enabled:=False;

  ToggleBoxProcessAllDuplicates.Checked:=False;
  ToggleBoxProcessAllDuplicates.Enabled:=False;
  //ToggleBoxBatchMode.Enabled:=False;
end;

procedure TForm1Main.UnGreyFormButtons(Sender: TObject);
begin
  ToggleBoxVerbose.Enabled:=True;
  ToggleBoxExtractFilesNormal.Enabled:=True;
  ToggleBoxExtractWithoutPaths.Enabled:=True;
  ToggleBoxExtractEmbedded.Enabled:=True;
  ToggleBoxTestFiles.Enabled:=True;
  DirectoryEditExtractFolder.Enabled:=True;
  EditPasswordBox.Enabled:=True;
  FileNameEditPasswordFile.Enabled:=True;
  ToggleBoxNoNewVersions.Enabled:=True;
  ToggleBoxProcessAllDuplicates.Enabled:=True;
  ReadINI();  //Restore Settings
  //ToggleBoxBatchMode.Enabled:=True;
end;

procedure WriteINI();
var
  appINI : TIniFile;
  filePath: String;
begin
   filePath:='';
   filePath:=GetSpecialFolderPath(CSIDL_APPDATA)+'\'+ApplicationName+'\';
   if not DirectoryExists(filePath) then CreateDir(filePath);
   filePath:=filePath + ApplicationName + '.ini';
appINI := TIniFile.Create(filePath);
 try
    appINI.WriteString('Options','Inno File Name',                  Form1Main.FileNameEditFile.FileName);
    appINI.WriteBool  ('Options','Verbose Only',                    Form1Main.ToggleBoxVerbose.Checked);
    appINI.WriteBool  ('Options','Extract File',                    Form1Main.ToggleBoxExtractFilesNormal.Checked);
    appINI.WriteBool  ('Options','Extract Without Paths',           Form1Main.ToggleBoxExtractWithoutPaths.Checked);
    appINI.WriteBool  ('Options','Extract Embedded Files',          Form1Main.ToggleBoxExtractEmbedded.Checked);
    appINI.WriteBool  ('Options','Test File Integrity',             Form1Main.ToggleBoxTestFiles.Checked);
    appINI.WriteString('Options','Extract To Folder Path/Filename', Form1Main.DirectoryEditExtractFolder.Directory);
    appINI.WriteString('Options','Password',                        Form1Main.EditPasswordBox.Text);
    appINI.WriteString('Options','Password Path/Filename',          Form1Main.FileNameEditPasswordFile.FileName);
    appINI.WriteBool  ('Options','No New Versions',                 Form1Main.ToggleBoxNoNewVersions.Checked);
    appINI.WriteBool  ('Options','Process All Duplicates',          Form1Main.ToggleBoxProcessAllDuplicates.Checked);
    appINI.WriteBool  ('Options','Batch Mode',                      Form1Main.ToggleBoxBatchMode.Checked);
    appINI.WriteBool  ('Options','File Info Only',                  Form1Main.ToggleBoxFileInfoOnly.Checked);
    appINI.WriteBool  ('Options','No Warnings',                     Form1Main.CheckBoxNoWarnings.Checked);
    with appINI, Form1Main do
      begin
        WriteInteger('Placement','Top', Top) ;
        WriteInteger('Placement','Left', Left) ;
        //WriteInteger('Placement','Width', Width) ;
        //WriteInteger('Placement','Height', Height) ;
      end;
 finally
   appIni.Free;
 end;
end;

procedure ReadINI();
var
 filePath: String;
 appINI : TIniFile;
begin
   filePath:='';
   filePath:=GetSpecialFolderPath(CSIDL_APPDATA)+'\'+ApplicationName+'\';
   if not DirectoryExists(filePath) then CreateDir(filePath);
   filePath:=filePath + ApplicationName + '.ini';
   appINI := TIniFile.Create(filePath);
  try
    Form1Main.FileNameEditFile.FileName :=             appINI.ReadString('Options','Inno File Name', '');
    Form1Main.ToggleBoxVerbose.Checked :=              appINI.ReadBool  ('Options','Verbose Only', False);
    Form1Main.ToggleBoxExtractFilesNormal.Checked :=   appINI.ReadBool  ('Options','Extract File', True);
    Form1Main.ToggleBoxExtractWithoutPaths.Checked :=  appINI.ReadBool  ('Options','Extract Without Paths', False);
    Form1Main.ToggleBoxExtractEmbedded.Checked :=      appINI.ReadBool  ('Options','Extract Embedded Files', True);
    Form1Main.ToggleBoxTestFiles.Checked :=            appINI.ReadBool  ('Options','Test File Integrity', True);
    Form1Main.DirectoryEditExtractFolder.Directory :=  appINI.ReadString('Options','Extract To Folder Path/Filename', '');
    Form1Main.EditPasswordBox.Text :=                  appINI.ReadString('Options','Password', '');
    Form1Main.FileNameEditPasswordFile.FileName :=     appINI.ReadString('Options','Password Path/Filename', '');
    Form1Main.ToggleBoxNoNewVersions.Checked :=        appINI.ReadBool  ('Options','No New Versions', False);
    Form1Main.ToggleBoxProcessAllDuplicates.Checked := appINI.ReadBool  ('Options','Process All Duplicates', False);
    Form1Main.ToggleBoxBatchMode.Checked :=            appINI.ReadBool  ('Options','Batch Mode', False);
    if (FileInfoOnly=True) then
      Form1Main.ToggleBoxFileInfoOnly.Checked :=       appINI.ReadBool  ('Options','File Info Only', False);
    Form1Main.CheckBoxNoWarnings.Checked :=            appINI.ReadBool  ('Options','No Warnings', False);

    with appINI, Form1Main do
    begin
      Top :=    appINI.ReadInteger('Placement','Top', Top) ;
      Left :=   appINI.ReadInteger('Placement','Left', Left);
      //Width :=  appINI.ReadInteger('Placement','Width', Width);
      //Height := appINI.ReadInteger('Placement','Height', Height);
    end;
  finally
    appINI.Free;
  end;
end;


end.

