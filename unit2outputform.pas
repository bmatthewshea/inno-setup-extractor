unit Unit2OutputForm;

{$mode objfpc}{$LONGSTRINGS ON}

interface

uses
  Classes, SysUtils, LResources, FileUtil, Forms, Controls, Graphics, Dialogs,
  Menus, Buttons, LCLType, ExtCtrls, ActnList, EditBtn, StdCtrls, ComCtrls,
  Unit4FileOps, Process;

procedure ExecInno(sExeName, sExeOptions : String; bNoWarnings : Boolean; iLines : Integer);

type

  { TFormSerialOutput }

  { TForm2Output }

  TForm2Output = class(TForm)
    ButtonCopyForm: TButton;
    ButtonClearForm: TButton;
    ButtonOutputSave: TButton;
    ButtonOutputOkClose: TButton;
    MemoOutput: TMemo;
    ProgressBarUnpack: TProgressBar;
    procedure ButtonCancelFormClick(Sender: TObject);
    procedure ButtonClearFormClick(Sender: TObject);
    procedure ButtonCopyFormClick(Sender: TObject);
    procedure ButtonOutputOkCloseClick(Sender: TObject);
    procedure ButtonOutputSaveClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure GreyFormButtons(Sender: TObject);
    procedure UnGreyFormButtons(Sender: TObject);
    function  ReturnList:TStringList;
  private
    { private declarations }
    //function MessageDlg(const Msg: string; DlgType: TMsgDlgType;
    //  Buttons: TMsgDlgButtons; HelpCtx: Integer): Integer; //make messageboxes appear over this form
  public
    { public declarations }

  end; 

var
  Form2Output: TForm2Output;

implementation

{$R *.lfm}

function TForm2Output.ReturnList:TStringList;
begin
  result := TStringList.Create;
  result.add(MemoOutput.Text);
  //result.add('');
end;

//function TForm2Output.MessageDlg(const Msg: string; DlgType: TMsgDlgType;
//  Buttons: TMsgDlgButtons; HelpCtx: Integer): Integer;
//begin
// with CreateMessageDialog(Msg, DlgType, Buttons) do
//    try
//      Position := poOwnerFormCenter;
//      Result := ShowModal
//    finally
//      Free
//    end
//end;


{ TForm2Output }

procedure TForm2Output.ButtonOutputOkCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TForm2Output.ButtonClearFormClick(Sender: TObject);
begin
   MemoOutput.Clear;
   //Form2Output.ButtonOutputSave.Enabled:=False;
   //Form2Output.ButtonCopyForm.Enabled:=False;
   //Form2Output.ButtonClearForm.Enabled:=False;
   GreyFormButtons(Sender);
end;

procedure TForm2Output.ButtonCancelFormClick(Sender: TObject);
begin

end;

procedure TForm2Output.ButtonCopyFormClick(Sender: TObject);
begin
  MemoOutput.SelectAll;
  MemoOutput.SetFocus;
  MemoOutput.CopyToClipboard;
end;

procedure TForm2Output.ButtonOutputSaveClick(Sender: TObject);
begin
  //ShowMessage(Form2Output.ReturnList.Text);
  SaveMyFile(Form2Output.ReturnList);
end;

procedure TForm2Output.FormClose(Sender: TObject; var CloseAction: TCloseAction
  );
begin
 Application.MainForm.BringToFront;
end;

procedure TForm2Output.FormCreate(Sender: TObject);
begin
  //DockMaster.MakeDockable(Form2Output);
  //DockMaster.MakeDockSite(Self,[akTop],admrpChild);
end;

procedure TForm2Output.GreyFormButtons(Sender: TObject);
begin
   Form2Output.ButtonOutputSave.Enabled:=False;
   Form2Output.ButtonCopyForm.Enabled:=False;
   Form2Output.ButtonClearForm.Enabled:=False;
end;

procedure TForm2Output.UnGreyFormButtons(Sender: TObject);
begin
   Form2Output.ButtonOutputSave.Enabled:=True;
   Form2Output.ButtonCopyForm.Enabled:=True;
   Form2Output.ButtonClearForm.Enabled:=True;
end;

procedure ExecInno(sExeName, sExeOptions : String; bNoWarnings : Boolean; iLines : Integer);
const
  C_BUFSIZE = 2048;
//    C_BUFSIZE = 2048;
var
  P: TProcess;
  Buffer: pointer;
  SStream: TStringStream;
  nread: longint;
  sExecutable: String;
  sOptions: String;
  fOutputFile : TextFile;
  Reply, BoxStyle, iStep : integer;
//  sPipeExecutable: String;
//  fOutputFile : TextFile;
begin
  sExecutable:='innounp.exe';
  iStep:=13;
  //sOptions:=' -x -d' + sExeName + '-Setup'; //important - leave space before option...
  //sOptions:=' -v'; //important - leave space before option...
  //Form2Output.ProgressBarUnpack.Step:=(iLines*2);
  Form2Output.ProgressBarUnpack.Max:=iLines;
  Form2Output.ProgressBarUnpack.Step:=iStep;
  //ShowMessage(IntToStr(iLines));
  P := TProcess.Create(nil);
  P.CommandLine := sExecutable + sExeOptions + sExeName;
  Form2Output.MemoOutput.Text:='Executing: '+#13#10+P.CommandLine+#13#10#13#10;
  // Confirm execution-
  //ShowMessage(BoolToStr(bNoWarnings, True));
  if (bNoWarnings = False) AND (Pos('-b',sExeOptions)=0)  then
  begin
    //if MessageDlg('Execute?', 'Execute command line?' + #13#10#13#10 + P.CommandLine, mtConfirmation,[mbOk,mbCancel],0)=mrCancel then exit;
    if Application.MessageBox(PChar('Execute command line?'+ #13#10#13#10 + P.CommandLine), 'Execute?', MB_ICONQUESTION + MB_YESNO) = IDNO then
    begin
      Exit;
      //Form2Output.ButtonOutputOkCloseClick(Sender);
    end;
  end;

  //
  P.Options := [poUsePipes, poStderrToOutPut, poNoConsole];
  // Prepare for capturing output
  Getmem(Buffer, C_BUFSIZE);
  SStream := TStringStream.Create('');
  // Start capturing output
  //Form2Output.MemoOutput.Lines.BeginUpdate;
  P.Execute;
  while P.Running do
  begin
    nread := P.Output.Read(Buffer^, C_BUFSIZE);
    if nread = 0 then
      sleep(100)
    else
      begin
        // Translate raw input to a string
        SStream.size := 0;
        SStream.Write(Buffer^, nread);
        // And add the raw stringdata to the memo
        Form2Output.MemoOutput.Lines.Text := Form2Output.MemoOutput.Lines.Text + SStream.DataString;
      end;

  Form2Output.MemoOutput.SelStart := Length(Form2Output.MemoOutput.Lines.Text)-1;
  Form2Output.ProgressBarUnpack.Position:= Form2Output.ProgressBarUnpack.Position + iStep;
  //Form2Output.ProgressBarUnpack.Position:= Round(iLines div SendMessage(Form2Output.MemoOutput.Handle, EM_LINEFROMCHAR, Form2Output.MemoOutput.Selstart, 0));
  Application.ProcessMessages;
  end;

  // Capture remainder of the output
  repeat
    nread := P.Output.Read(Buffer^, C_BUFSIZE);
    if nread > 0 then
    begin
      SStream.size := 0;
      SStream.Write(Buffer^, nread);
      Form2Output.MemoOutput.Lines.Text := Form2Output.MemoOutput.Lines.Text + SStream.Datastring;
    end
  until nread = 0;
  //  Form2Output.MemoOutput.Lines.Text := Form2Output.MemoOutput.Lines.Text + #13#10 + 'Files Listed = ' + IntToStr(Form2Output.MemoOutput.Lines.Count-7);
  Form2Output.MemoOutput.SelStart := Length(Form2Output.MemoOutput.Lines.Text);
//  Form2Output.ProgressBarUnpack.Position:= Form2Output.ProgressBarUnpack.Position + 1;
  Application.ProcessMessages;

    // Clean up
  P.Free;
  Freemem(buffer);
  SStream.Free;
end;

end.

