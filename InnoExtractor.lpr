program InnoExtractor;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Unit1MainForm, Unit2OutputForm, Unit3AboutForm, Unit4FileOps;

{$R *.res}

begin
  Application.Title:='Inno Setup Extractor';
  Application.Initialize;
  Application.CreateForm(TForm1Main, Form1Main);
  Application.CreateForm(TForm2Output, Form2Output);
  Application.CreateForm(TForm3About, Form3About);
  Application.Run;
end.

